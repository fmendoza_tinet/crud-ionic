import { NgModule } from '@angular/core';
import { TasksPage } from './tasks';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [TasksPage],
  imports: [IonicPageModule.forChild(TasksPage)],
  entryComponents: [TasksPage]
})
export class TasksPageModule { }