import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { DbtaskserviceProvider } from '../../providers/dbtaskservice/dbtaskservice';
import { Task } from '../../model/Task';
import { IonicPage } from 'ionic-angular';

/**
 * Generated class for the TasksPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html',
})
export class TasksPage {

  allTask: Task[] = [];
  title: string;
  id: string;

  constructor(
    public navCtrl: NavController,
    public loadincontroller: LoadingController,
    public _dbtaskservice: DbtaskserviceProvider,
    public _toast: ToastController) {
  }

  ionViewDidLoad() {
    let loadingdata = this.loadincontroller.create({
      content: "Loading Tasks..."
    });

    loadingdata.present();

    this._dbtaskservice.getAllTask().subscribe(
      (data: Task[]) => {
        this.allTask = data;
      },
      function (error) {
        console.log("error" + error)
      },
      function () {
        loadingdata.dismiss();
      }
    );
  }

  addTask() {
    let loadingdata = this.loadincontroller.create({
      content: "Posting Tasks..."
    });

    loadingdata.present();

    this._dbtaskservice.addTask(new Task(this.id, this.title, 'pending'))
      .subscribe(
      (data: Task) => {
        if (data != null) {
          this.allTask.push(new Task(this.id, this.title, 'pending'));
          this.title = '';
          this.id = '';
        }
      },

      function (error) { },
      function () {
        loadingdata.dismiss();
      }
      );
  }

  updateTask(t: Task) {
    if (t.Status == 'done') {
      t.Status = 'pending';
    } else {
      t.Status = 'done'
    }
    this._dbtaskservice.editTask(t).subscribe(
      (data: any) => {
        if (data.affectedRows == 1) {
          let mes = this._toast.create({
            message: 'Task Updated Successfully',
            duration: 2000,
            position: 'bottom'
          });
          mes.present();
        } else {
          let mes = this._toast.create({
            message: 'Error in Updating',
            duration: 2000,
            position: 'bottom'
          });
          mes.present();
        }
      }
    );
  }

  deleteTask(t: Task) {
    this._dbtaskservice.deleteTask(t).subscribe(
      (data: any) => {
        if (data.affectedRows == 1) {
          let mes = this._toast.create({
            message: 'Task Deleted Successfully',
            duration: 2000,
            position: 'bottom'
          });
          this.allTask.splice(this.allTask.indexOf(t), 1);
          mes.present();
        } else {
          let mes = this._toast.create({
            message: 'Error in deleting task',
            duration: 2000,
            position: 'bottom'
          });
          mes.present();
        }
      }
    );
  }
}
